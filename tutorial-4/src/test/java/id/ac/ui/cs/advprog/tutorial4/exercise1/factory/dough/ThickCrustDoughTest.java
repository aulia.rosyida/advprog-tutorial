package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;

public class ThickCrustDoughTest {

    private Class<?> thickCrustClass;


    @Before
    public void setUp() throws Exception {
        thickCrustClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough");
    }

    @Test
    public void testThickCrustIsADough() {
        Collection<Type> classInterface = Arrays.asList(thickCrustClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough"))
        );
    }

    @Test
    public void testThickCrustOverrideToStringMethod() throws Exception {
        Method toStr = thickCrustClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToString()
    {
        ThickCrustDough thickDough = new ThickCrustDough();
        String expected = "ThickCrust style extra thick crust dough";
        assertEquals(expected, thickDough.toString());
    }
}