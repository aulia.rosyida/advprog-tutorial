package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.WensleydaleCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.PacificClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.MixCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ExtraHotSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Tomato;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import org.junit.Before;
import org.junit.Test;

public class DepokPizzaIngredientFactoryTest {

    private DepokPizzaIngredientFactory depokFactory;

    @Before
    public void setUp() throws Exception {
        depokFactory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testDepokFactoryCanCreateSpecificDough() {
        assertEquals((depokFactory.createDough() instanceof MixCrustDough), true);
    }

    @Test
    public void testDepokFactoryCanCreateSpecificSauce() {
        assertEquals((depokFactory.createSauce() instanceof ExtraHotSauce),true);
    }

    @Test
    public void testDepokFactoryCanCreateSpecificCheese() {
        assertEquals((depokFactory.createCheese() instanceof WensleydaleCheese),true);
    }

    @Test
    public void testNewYorkStoreCanCreateMatchVeggies() {
        Veggies[] creates = depokFactory.createVeggies();
        assertEquals((creates[0] instanceof BlackOlives),true);
        assertEquals((creates[1] instanceof Eggplant),true);
        assertEquals((creates[2] instanceof Spinach),true);
        assertEquals((creates[3] instanceof Tomato),true);
    }

    @Test
    public void testNewYorkStoreCanCreateMatchClam() {
        assertEquals((depokFactory.createClam() instanceof PacificClams),true);
    }

}