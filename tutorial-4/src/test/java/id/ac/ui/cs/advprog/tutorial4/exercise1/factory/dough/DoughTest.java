package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import java.lang.reflect.Modifier;
import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class DoughTest {

    private Class<?> doughClass;

    @Before
    public void setUp() throws Exception {
        doughClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough");
    }

    @Test
    public void testDoughIsAPublicInterface() {
        int classModifiers = doughClass.getModifiers();

        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testDoughHasAbstractMethod() throws Exception {
        Method toStr = doughClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}