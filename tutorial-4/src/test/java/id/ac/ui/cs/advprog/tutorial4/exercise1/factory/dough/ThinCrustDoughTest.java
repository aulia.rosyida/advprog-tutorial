package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;

public class ThinCrustDoughTest {

    private Class<?> thinCrustClass;

    @Before
    public void setUp() throws Exception {
        thinCrustClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough");
    }

    @Test
    public void testThinCrustIsADough() {
        Collection<Type> classInterface = Arrays.asList(thinCrustClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough"))
        );
    }

    @Test
    public void testThinCrustOverrideToStringMethod() throws Exception {
        Method toStr = thinCrustClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToString()
    {
        ThinCrustDough thinDough = new ThinCrustDough();
        String expected = "Thin Crust Dough";
        assertEquals(expected, thinDough.toString());
    }
}