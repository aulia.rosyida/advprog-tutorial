package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.lang.reflect.Modifier;
import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

public class CheesePizzaTest {

    private Class<?> cheesePizza;

    @Before
    public void setUp() throws Exception {
        cheesePizza = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza");
    }

    @Test
    public void testCheesePizzaIsPizza() throws Exception {
        Class<?> parent = cheesePizza.getSuperclass();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza", parent.getName());
    }

    @Test
    public void testCheesePizzaOverridePrepare() throws Exception {
        Method prepare = cheesePizza.getDeclaredMethod("prepare");
        int methodModifiers = prepare.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", prepare.getGenericReturnType().getTypeName());
    }

}