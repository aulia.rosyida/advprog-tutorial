package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;

public class BlackOlivesTest {

    private Class<?> blackOlivesClass;

    @Before
    public void setUp() throws Exception {
        blackOlivesClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives");
    }

    @Test
    public void testBlackOlivesIsAVeggie() {
        Collection<Type> classInterface = Arrays.asList(blackOlivesClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testBlackOlivesOverrideToStringMethod() throws Exception {
        Method toStr = blackOlivesClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToString()
    {
        BlackOlives blackObj = new BlackOlives();
        String expected = "Black Olives";
        assertEquals(expected, blackObj.toString());
    }

}