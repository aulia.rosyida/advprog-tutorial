package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;

public class ReggianoCheeseTest {

    private Class<?> reggianoClass;

    @Before
    public void setUp() throws Exception {
        reggianoClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese");
    }

    @Test
    public void testReggianoIsACheese() {
        Collection<Type> classInterface = Arrays.asList(reggianoClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testReggianoOverrideToStringMethod() throws Exception {
        Method toStr = reggianoClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testReggianoHasSpecificToString()
    {
        ReggianoCheese reggie = new ReggianoCheese();
        String expected = "just ordinary cheese";
        assertNotEquals(expected, reggie.toString());
    }
}