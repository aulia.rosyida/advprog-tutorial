package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;

public class ExtraHotSauceTest {

    private Class<?> extraHotClass;

    @Before
    public void setUp() throws Exception {
        extraHotClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ExtraHotSauce");
    }

    @Test
    public void testExtraHotIsASauce() {
        Collection<Type> classInterface = Arrays.asList(extraHotClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce"))
        );
    }

    @Test
    public void testExtraHotOverrideToStringMethod() throws Exception {
        Method toStr = extraHotClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToString()
    {
        ExtraHotSauce sausHot = new ExtraHotSauce();
        String expected = "Extra Hot sauce is so spicy :O";
        assertEquals(expected, sausHot.toString());
    }
}