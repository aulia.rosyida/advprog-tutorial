package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Collection;
import java.util.Arrays;

public class WensleydaleCheeseTest {

    private Class<?> WensleydaleClass;

    @Before
    public void setUp() throws Exception {
        WensleydaleClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.WensleydaleCheese");
    }

    @Test
    public void testWensleydaleIsACheese() {
        Collection<Type> classInterface = Arrays.asList(WensleydaleClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testWensleydaleOverrideToStringMethod() throws Exception {
        Method toStr = WensleydaleClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testFrozenClamsHasSpecificToString()
    {
        WensleydaleCheese wensley = new WensleydaleCheese();
        String expected = "just ordinary cheese";
        assertNotEquals(expected, wensley.toString());
    }
}