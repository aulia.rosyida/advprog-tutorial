package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;


public class OnionTest {

    private Class<?> onionClass;

    @Before
    public void setUp() throws Exception {
        onionClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion");
    }

    @Test
    public void testOnionIsAVeggie() {
        Collection<Type> classInterface = Arrays.asList(onionClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testOnionOverrideToStringMethod() throws Exception {
        Method toStr = onionClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToString()
    {
        Onion bawangObj = new Onion();
        String expected = "Onion";
        assertEquals(expected, bawangObj.toString());
    }
}