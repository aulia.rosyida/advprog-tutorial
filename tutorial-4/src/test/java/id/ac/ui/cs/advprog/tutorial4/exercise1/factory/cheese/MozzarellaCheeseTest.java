package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;


public class MozzarellaCheeseTest {

    private Class<?> mozzarellaClass;

    @Before
    public void setUp() throws Exception {
        mozzarellaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese");
    }

    @Test
    public void testMozzarellaCheeseIsACheese() {
        Collection<Type> classInterface = Arrays.asList(mozzarellaClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testMozzarellaOverrideToStringMethod() throws Exception {
        Method toStr = mozzarellaClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMozzarellaHasSpecificToString()
    {
        MozzarellaCheese mozzaCheese = new MozzarellaCheese();
        String expected = "just ordinary cheese";
        assertNotEquals(expected, mozzaCheese.toString());
    }

}