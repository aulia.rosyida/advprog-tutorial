package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;

public class MixCrustDoughTest {

    private Class<?> mixClass;

    @Before
    public void setUp() throws Exception {
        mixClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.MixCrustDough");
    }

    @Test
    public void testMixCrustIsADough() {
        Collection<Type> classInterface = Arrays.asList(mixClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough"))
        );
    }

    @Test
    public void testMixCrustOverrideToStringMethod() throws Exception {
        Method toStr = mixClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToString()
    {
        MixCrustDough mixDough = new MixCrustDough();
        String expected = "MixCrust style extra thick n' thin crust dough";
        assertEquals(expected, mixDough.toString());
    }
}