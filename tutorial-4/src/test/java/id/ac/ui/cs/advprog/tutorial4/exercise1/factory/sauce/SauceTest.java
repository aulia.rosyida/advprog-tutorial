package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import java.lang.reflect.Modifier;
import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SauceTest {

    private Class<?> sauceClass;

    @Before
    public void setUp() throws Exception {
        sauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce");
    }

    @Test
    public void testSauceIsAPublicInterface() {
        int classModifiers = sauceClass.getModifiers();

        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testSauceHasAbstractMethod() throws Exception {
        Method toStr = sauceClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}