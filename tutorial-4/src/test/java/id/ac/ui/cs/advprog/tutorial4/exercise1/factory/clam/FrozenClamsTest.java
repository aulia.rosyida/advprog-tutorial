package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;

public class FrozenClamsTest {

    private Class<?> frozenClamsClass;

    @Before
    public void setUp() throws Exception {
        frozenClamsClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams");
    }

    @Test
    public void testFrozenClamsIsAClam() {
        Collection<Type> classInterface = Arrays.asList(frozenClamsClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams"))
        );
    }

    @Test
    public void testFrozenClamsOverrideToStringMethod() throws Exception {
        Method toStr = frozenClamsClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testFrozenClamsHasSpecificToString()
    {
        FrozenClams frozenObj = new FrozenClams();
        String expected = "just ordinary clam";
        assertNotEquals(expected, frozenObj.toString());
    }
}