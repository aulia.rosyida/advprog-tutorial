package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;


public class RedPepperTest {

    private Class<?> redPepperClass;

    @Before
    public void setUp() throws Exception {
        redPepperClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper");
    }

    @Test
    public void testRedPepperIsAVeggie() {
        Collection<Type> classInterface = Arrays.asList(redPepperClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testRedPepperOverrideToStringMethod() throws Exception {
        Method toStr = redPepperClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToString()
    {
        RedPepper redObj = new RedPepper();
        String expected = "Red Pepper";
        assertEquals(expected, redObj.toString());
    }
}