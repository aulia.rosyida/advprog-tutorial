package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;

public class MushroomTest {

    private Class<?> mushroomClass;

    @Before
    public void setUp() throws Exception {
        mushroomClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom");
    }

    @Test
    public void testMushroomIsAVeggie() {
        Collection<Type> classInterface = Arrays.asList(mushroomClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testMushroomOverrideToStringMethod() throws Exception {
        Method toStr = mushroomClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToString()
    {
        Mushroom jamur = new Mushroom();
        String expected = "Mushrooms";
        assertEquals(expected, jamur.toString());
    }
}