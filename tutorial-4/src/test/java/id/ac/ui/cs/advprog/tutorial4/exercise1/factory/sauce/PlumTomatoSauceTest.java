package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;

public class PlumTomatoSauceTest {

    private Class<?> tomatoSauceClass;

    @Before
    public void setUp() throws Exception {
        tomatoSauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce");
    }

    @Test
    public void testPlumTomatoIsASauce() {
        Collection<Type> classInterface = Arrays.asList(tomatoSauceClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce"))
        );
    }

    @Test
    public void testPlumTomatoOverrideToStringMethod() throws Exception {
        Method toStr = tomatoSauceClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToString()
    {
        PlumTomatoSauce sausPlum = new PlumTomatoSauce();
        String expected = "Tomato sauce with plum tomatoes";
        assertEquals(expected, sausPlum.toString());
    }
}