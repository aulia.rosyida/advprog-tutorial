package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;

public class TomatoTest {

    private Class<?> tomatoCLass;

    @Before
    public void setUp() throws Exception {
        tomatoCLass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper");
    }

    @Test
    public void testTomatoIsAVeggie() {
        Collection<Type> classInterface = Arrays.asList(tomatoCLass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testTomatoOverrideToStringMethod() throws Exception {
        Method toStr = tomatoCLass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToString()
    {
        Tomato tomatoObj = new Tomato();
        String expected = "Tomato";
        assertEquals(expected, tomatoObj.toString());
    }
}