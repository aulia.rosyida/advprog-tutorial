package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import java.lang.reflect.Modifier;
import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ClamsTest {

    private Class<?> clamsClass;

    @Before
    public void setUp() throws Exception {
        clamsClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams");
    }

    @Test
    public void testClamsIsAPublicInterface() {
        int classModifiers = clamsClass.getModifiers();

        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testClamsHasAbstractMethod() throws Exception {
        Method toStr = clamsClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

}