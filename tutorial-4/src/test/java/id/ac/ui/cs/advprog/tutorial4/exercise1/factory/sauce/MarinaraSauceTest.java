package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;

public class MarinaraSauceTest {

    private Class<?> marinaraClass;

    @Before
    public void setUp() throws Exception {
        marinaraClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce");
    }

    @Test
    public void testMarinaraIsASauce() {
        Collection<Type> classInterface = Arrays.asList(marinaraClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce"))
        );
    }

    @Test
    public void testMarinaraOverrideToStringMethod() throws Exception {
        Method toStr = marinaraClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToString()
    {
        MarinaraSauce sausMa = new MarinaraSauce();
        String expected = "Marinara Sauce";
        assertEquals(expected, sausMa.toString());
    }
}