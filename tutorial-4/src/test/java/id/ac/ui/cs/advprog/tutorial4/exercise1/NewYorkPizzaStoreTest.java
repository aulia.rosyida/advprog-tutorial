package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class NewYorkPizzaStoreTest {

    NewYorkPizzaStore nyStore;

    @Before
    public void setUp() throws Exception {
        nyStore = new NewYorkPizzaStore();
    }

    @Test
    public void testPizzaNotNull() {
        Pizza piza = nyStore.createPizza("veggie");

        assertNotNull(piza);

    }

    @Test
    public void testCheesePizza() {
        Pizza piza = nyStore.createPizza("cheese");

        assertEquals("New York Style Cheese Pizza", piza.getName());

    }

    @Test
    public void testVeggiePizza() {
        Pizza piza = nyStore.createPizza("veggie");

        assertEquals("New York Style Veggie Pizza", piza.getName());

    }

    @Test
    public void testClamPizza() {
        Pizza piza = nyStore.createPizza("clam");

        assertEquals("New York Style Clam Pizza", piza.getName());

    }

    @Test
    public void testOrderPizza() {
        Pizza piza = nyStore.orderPizza("clam");
        assertEquals("New York Style Clam Pizza", piza.getName());
    }
}