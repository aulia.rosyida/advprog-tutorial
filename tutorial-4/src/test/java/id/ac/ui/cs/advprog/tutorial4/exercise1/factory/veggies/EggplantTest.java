package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;

public class EggplantTest {

    private Class<?> eggplantClass;

    @Before
    public void setUp() throws Exception {
        eggplantClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant");
    }

    @Test
    public void testEggplantIsAVeggie() {
        Collection<Type> classInterface = Arrays.asList(eggplantClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testEggplantOverrideToStringMethod() throws Exception {
        Method toStr = eggplantClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToString()
    {
        Eggplant eggplantObj = new Eggplant();
        String expected = "Eggplant";
        assertEquals(expected, eggplantObj.toString());
    }
}