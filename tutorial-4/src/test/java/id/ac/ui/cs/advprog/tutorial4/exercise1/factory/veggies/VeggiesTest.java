package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import java.lang.reflect.Modifier;
import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class VeggiesTest {

    private Class<?> veggiesClass;

    @Before
    public void setUp() throws Exception {
        veggiesClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies");
    }

    @Test
    public void testVeggiesIsAPublicInterface() {
        int classModifiers = veggiesClass.getModifiers();

        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testVeggiesHasAbstractMethod() throws Exception {
        Method toStr = veggiesClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}