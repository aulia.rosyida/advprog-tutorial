package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Collection;
import java.util.Arrays;

public class FreshClamsTest {

    private Class<?> freshClamsClass;

    @Before
    public void setUp() throws Exception {
        freshClamsClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams");
    }

    @Test
    public void testFreshClamsIsAClam() {
        Collection<Type> classInterface = Arrays.asList(freshClamsClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams"))
        );
    }

    @Test
    public void testFreshClamsOverrideToStringMethod() throws Exception {
        Method toStr = freshClamsClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testFreshClamsHasSpecificToString()
    {
        FreshClams freshObj = new FreshClams();
        String expected = "just ordinary clam";
        assertNotEquals(expected, freshObj.toString());
    }
}