package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;

public class SpinachTest {

    private Class<?> spinachCLass;

    @Before
    public void setUp() throws Exception {
        spinachCLass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper");
    }

    @Test
    public void testSpinachIsAVeggie() {
        Collection<Type> classInterface = Arrays.asList(spinachCLass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testSpinachOverrideToStringMethod() throws Exception {
        Method toStr = spinachCLass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testToString()
    {
        Spinach bayamObj = new Spinach();
        String expected = "Spinach";
        assertEquals(expected, bayamObj.toString());
    }
}