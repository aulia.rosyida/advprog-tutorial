package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import org.junit.Before;
import org.junit.Test;


public class NewYorkPizzaIngredientFactoryTest {

    private NewYorkPizzaIngredientFactory nyFactory;

    @Before
    public void setUp() throws Exception {
        nyFactory = new NewYorkPizzaIngredientFactory();
    }

    @Test
    public void testNewYorkStoreCanCreateMatchDough() {
        assertEquals((nyFactory.createDough() instanceof ThinCrustDough), true);
    }

    @Test
    public void testNewYorkStoreCanCreateMatchSauce() {
        assertEquals((nyFactory.createSauce() instanceof MarinaraSauce),true);
    }

    @Test
    public void testNewYorkStoreCanCreateMatchCheese() {
        assertEquals((nyFactory.createCheese() instanceof ReggianoCheese),true);
    }

    @Test
    public void testNewYorkStoreCanCreateMatchVeggies() {
        Veggies[] creates = nyFactory.createVeggies();
        assertEquals((creates[0] instanceof Garlic),true);
        assertEquals((creates[1] instanceof Onion),true);
        assertEquals((creates[2] instanceof Mushroom),true);
        assertEquals((creates[3] instanceof RedPepper),true);
    }

    @Test
    public void testNewYorkStoreCanCreateMatchClam() {
        assertEquals((nyFactory.createClam() instanceof FreshClams),true);
    }

}