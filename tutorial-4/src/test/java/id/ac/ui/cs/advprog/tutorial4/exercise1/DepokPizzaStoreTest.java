package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DepokPizzaStoreTest {
    DepokPizzaStore dpkStore;

    @Before
    public void setUp() throws Exception {
        dpkStore = new DepokPizzaStore();
    }

    @Test
    public void testPizzaNotNull() {
        Pizza piza = dpkStore.createPizza("clam");

        assertNotNull(piza);

    }

    @Test
    public void testCheesePizza() {
        Pizza piza = dpkStore.createPizza("cheese");

        assertEquals("Depok Style Cheese Pizza", piza.getName());

    }

    @Test
    public void testVeggiePizza() {
        Pizza piza = dpkStore.createPizza("veggie");

        assertEquals("Depok Style Veggie Pizza", piza.getName());

    }

    @Test
    public void testClamPizza() {
        Pizza piza = dpkStore.createPizza("clam");

        assertEquals("Depok Style Clam Pizza", piza.getName());

    }

    @Test
    public void testOrderPizza() {
        Pizza piza = dpkStore.orderPizza("clam");
        assertEquals("Depok Style Clam Pizza", piza.getName());
    }

}