package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Arrays;


public class ParmesanCheeseTest {

    private Class<?> parmesanClass;

    @Before
    public void setUp() throws Exception {
        parmesanClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese");
    }

    @Test
    public void testParmesanIsACheese() {
        Collection<Type> classInterface = Arrays.asList(parmesanClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testParmesanOverrideToStringMethod() throws Exception {
        Method toStr = parmesanClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toStr.getGenericReturnType().getTypeName());
    }

    @Test
    public void testParmesanHasSpecificToString()
    {
        ParmesanCheese parmesan = new ParmesanCheese();
        String expected = "just ordinary cheese";
        assertNotEquals(expected, parmesan.toString());
    }

}