package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import java.lang.reflect.Modifier;
import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CheeseTest {

    private Class<?> cheeseClass;

    @Before
    public void setUp() throws Exception {
        cheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese");
    }

    @Test
    public void testCheeseIsAPublicInterface() {
        int classModifiers = cheeseClass.getModifiers();

        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testCheeseHasAbstractMethod() throws Exception {
        Method toStr = cheeseClass.getDeclaredMethod("toString");
        int methodModifiers = toStr.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

}