package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class ExtraHotSauce implements Sauce {
    public String toString() {
        return "Extra Hot sauce is so spicy :O";
    }
}
