package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class MixCrustDough implements Dough {

    public String toString() {
        return "MixCrust style extra thick n' thin crust dough";
    }
}
