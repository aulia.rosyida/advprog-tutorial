package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class PacificClams implements Clams {

    public String toString() {
        return "Pacific Clams, the most favorite Clam in Washington";
    }
}
