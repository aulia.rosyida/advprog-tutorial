package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class WensleydaleCheese implements Cheese {

    public String toString() {
        return "Wensleydale Cheese";
    }
}
