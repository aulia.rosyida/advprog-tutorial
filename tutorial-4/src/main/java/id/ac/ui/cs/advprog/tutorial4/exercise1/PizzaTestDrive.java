package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NewYorkPizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza + "\n");

        // Create a new Pizza Store franchise at Depok
        PizzaStore pribumiStore = new DepokPizzaStore();

        Pizza pizzaQ = pribumiStore.orderPizza("cheese");
        System.out.println("Aulia ordered a " + pizzaQ + "\n");

        pizzaQ = pribumiStore.orderPizza("clam");
        System.out.println("Aulia ordered a " + pizzaQ + "\n");

        pizzaQ = pribumiStore.orderPizza("veggie");
        System.out.println("Aulia ordered a " + pizzaQ + "\n");

    }
}
