package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.List;

public class CompanyMain {

    public static void main(String[] args) {

        Company company = new Company();

        Employees luffy = new Ceo("Luffy", 500000.00);
        company.addEmployee(luffy);

        Employees zorro = new Cto("Zorro", 320000.00);
        company.addEmployee(zorro);

        Employees franky = new BackendProgrammer("Franky", 94000.00);
        company.addEmployee(franky);

        Employees usopp = new BackendProgrammer("Usopp", 200000.00);
        company.addEmployee(usopp);

        Employees nami = new FrontendProgrammer("Nami", 66000.00);
        company.addEmployee(nami);

        Employees robin = new FrontendProgrammer("Robin", 130000.00);
        company.addEmployee(robin);

        Employees sanji = new UiUxDesigner("sanji", 177000.00);
        company.addEmployee(sanji);

        Employees brook = new NetworkExpert("Brook", 83000.00);
        company.addEmployee(brook);

        List<Employees> all = company.getAllEmployees();
        for (Employees anEmployee : all) {
            System.out.println(anEmployee.getName());
        }

    }
}