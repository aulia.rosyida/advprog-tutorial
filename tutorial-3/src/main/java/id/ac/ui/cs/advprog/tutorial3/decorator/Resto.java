package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cucumber;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Tomato;

public class Resto {

    public static void main(String[] args) {

        Food order1 = BreadProducer.THIN_BUN.createBreadToBeFilled();
        System.out.println(order1.cost());
        order1 = new Cucumber(new Lettuce(new ChickenMeat(new ChiliSauce(new Tomato(order1)))));
        System.out.println(order1.getDescription());
        System.out.println(order1.cost());
    }
}